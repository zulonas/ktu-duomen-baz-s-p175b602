SELECT
  `Products`.`id`,
  `Products`.`name`,
  `Products`.`fk_Manufacturers` as `manufacturer`,
  SUM(`OrderItems`.`quantity`) as `amount`,
  ROUND(
    SUM(
      `Products`.`price` * `OrderItems`.`quantity`
    ),
    2
  ) AS `totalSum`
FROM
  `Products`
  INNER JOIN `OrderItems` ON `Products`.`id` = `OrderItems`.`fk_Products`
WHERE
  `Products`.`fk_Manufacturers` = 11
GROUP BY
  `Products`.`id`
ORDER BY
  `amount` ASC



SELECT
  SUM(`OrderItems`.`quantity`) as `amount`,
  ROUND(SUM(`Products`.`price` * `OrderItems`.`quantity`), 2) AS `totalSum`
FROM
  `Products`
  INNER JOIN `OrderItems` ON `Products`.`id` = `OrderItems`.`fk_Products`
WHERE
  `Products`.`fk_Manufacturers` = 11

