<?php

// sukuriame sutarčių klasės objektą
include 'libraries/orders.class.php';
$ordersObj = new Orders();

// sukuriame puslapiavimo klasės objektą
include 'utils/paging.class.php';
$paging = new Paging(General::NUMBER_OF_ROWS_IN_PAGE);

// suformuojame sąrašo puslapius
$paging->process($ordersObj->getOrderCount(), $pageId);

// išrenkame nurodyto puslapio sutartis
$data = $ordersObj->getOrderList();

// įtraukiame šabloną
include 'templates/order_list.tpl.php';
