<?php

// sukuriame sutarčių klasės objektą
include 'libraries/manufacturer.class.php';
$usersObj = new Manufacturers();

// sukuriame puslapiavimo klasės objektą
include 'utils/paging.class.php';
$paging = new Paging(General::NUMBER_OF_ROWS_IN_PAGE);

// suformuojame sąrašo puslapius
$paging->process($usersObj->getManufacturerCount(), $pageId);

// išrenkame nurodyto puslapio sutartis
$data = $usersObj->getManufacturerList();

// įtraukiame šablon
include 'templates/manufacturer_list.tpl.php';
