<?php

include 'libraries/login.class.php';
$loginObj = new Login();
$formErrors = null;

$required = array('email', 'password');
$maxLengths = array (
	'email' => 253,
	'password' => 64
);

if ($_POST['submit'] === 'login') {
	include 'utils/validator.class.php';

	// field validation types
	$validations = array (
		'email' => 'email',
		'password' => 'alfanum');
	$validator = new validator($validations, $required, $maxLengths);

	// validation check
	if ($validator->validate($_POST)) {
		// data field
		$dataPrepared = $validator->preparePostFieldsForSQL();

		// įrašome naują klientą
		$hashedPassword = $loginObj->getHash($dataPrepared['email']);

		if (is_null($hashedPassword)) {
			$formErrors = "Netinkami duomenys";
		} elseif (password_verify($dataPrepared['password'], $hashedPassword['password'])) {
			//succesfuly loggedin
			session_start();

			// Store data in session variables
			$_SESSION["loggedin"] = true;
			$_SESSION["id"] = $hashedPassword['id'];
			$_SESSION["email"] = $dataPrepared['email'];

			//updating user last login data
			$loginObj->updateLastLogin($hashedPassword['id']);

			// redirect to front page
			common::redirect("index.php?module=front&action=page");
		} else {
			$formErrors = "Netinkami duomenys";
		}
	} else {
		// gauname klaidų pranešimą
		$formErrors = "Buvo neįvesti arba netinkamai įvesti šie laukai: " .
			$validator->getErrorHTML();
	}
}

//registration
if ($_POST['submit'] === 'register') {
	include 'utils/validator.class.php';

	// nustatome laukų validatorių tipus
	$validations = array (
		'email' => 'email',
		'password' => 'alfanum');

	// sukuriame laukų validatoriaus objektą
	$validator = new validator($validations, $required, $maxLengths);

	//laukai įvesti be klaidų
	if ($validator->validate($_POST)) {
		// suformuojame laukų reikšmių masyvą SQL užklausai
		$dataPrepared = $validator->preparePostFieldsForSQL();

		$hashedPassword = $loginObj->getHash($dataPrepared['email']);
		//tikriname ar toks email nėra jau registruotas
		if (is_null($hashedPassword)){
			// įrašome naują klientą
			$loginObj->registerUser($dataPrepared);

			// nukreipiame vartotoją į klientų puslapį
			common::redirect("index.php?module=user&action=login");
			die();
		} else {
			$formErrors = "Toks el. paštas jau yra registruotas";
		}
	} else {
		// gauname klaidų pranešimą
		$formErrors = "Buvo neįvesti arba netinkamai įvesti šie laukai: " .
			$validator->getErrorHTML();
	}
}

include 'templates/user_login.tpl.php';
