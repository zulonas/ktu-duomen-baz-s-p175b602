<?php

// sukuriame sutarčių klasės objektą
include 'libraries/users.class.php';
$usersObj = new Users();

// sukuriame puslapiavimo klasės objektą
include 'utils/paging.class.php';
$paging = new Paging(General::NUMBER_OF_ROWS_IN_PAGE);

// suformuojame sąrašo puslapius
$paging->process($usersObj->getUserCount(), $pageId);

// išrenkame nurodyto puslapio sutartis
$data = $usersObj->getUserList();

// įtraukiame šablon
include 'templates/user_list.tpl.php';
