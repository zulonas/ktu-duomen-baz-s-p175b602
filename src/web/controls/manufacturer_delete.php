<?php

include 'libraries/manufacturer.class.php';
$manufacturersObj = new Manufacturers();

if(!empty($id)) {
	$count = $manufacturersObj->getProductsCountOfManufacturer($id);

	$removeErrorParameter = "";
	if($count == 0) {
		$manufacturersObj->deleteManufacturer($id);
	} else {
		$removeErrorParameter = '&remove_error=1';
	}

	common::redirect("index.php?module={$module}&action=list{$removeErrorParameter}");
	die();
}
