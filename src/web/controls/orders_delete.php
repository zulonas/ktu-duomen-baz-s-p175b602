<?php

include 'libraries/orders.class.php';
$ordersObj = new Orders();

if(!empty($id)) {
	$ordersObj->deleteOrder($id);

	common::redirect("index.php?module={$module}&action=list");
	exit;
}

