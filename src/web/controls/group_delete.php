<?php

include 'libraries/group.class.php';
$groupsObj = new Groups();

if(!empty($id)) {
	$count = $groupsObj->getProductsCountOfGroup($id);
	
	$removeErrorParameter = "";
	if($count == 0) {
		$groupsObj->deleteGroup($id);
	} else {
		$removeErrorParameter = '&remove_error=1';
	}

	common::redirect("index.php?module={$module}&action=list{$removeErrorParameter}");
	die();
}
