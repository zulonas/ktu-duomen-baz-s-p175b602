<?php

include 'libraries/group.class.php';
$groupsObj = new Groups();

$data = array();
$formErrors = null;

// nustatome privalomus formos laukus
$required = array('name');

// maksimalūs leidžiami laukų ilgiai
$maxLengths = array (
	'name' => 50,
	'slug' => 15,
	'description' => 50,
	'attributes' => 50
);

// vartotojas paspaudė išsaugojimo mygtuką
if(!empty($_POST['submit'])) {
	include 'utils/validator.class.php';

	// nustatome laukų validatorių tipus
	$validations = array (
		'name' => 'alfanum',
		'slug' => 'alfanum',
		'description' => 'alfanum',
		'attributes' => 'alfanum'
	);

	// sukuriame laukų validatoriaus objektą
	$validator = new validator($validations, $required, $maxLengths);

	// laukai įvesti be klaidų
	if($validator->validate($_POST)) {
		// suformuojame laukų reikšmių masyvą SQL užklausai
		$dataPrepared = $validator->preparePostFieldsForSQL();

		// įrašome naują klientą
		$groupsObj->editGroup($dataPrepared);

		//// nukreipiame vartotoją į klientų puslapį
		common::redirect("index.php?module={$module}&action=list");
		die();
	}
	else {

		$formErrors = "Aprodojant duomenis įvyko klaida, patikrinkite:";
		$formErrors .= $validator->getErrorHTML();
	}
} else {
	$data = $groupsObj->getGroup($id);
}

// įtraukiame šabloną
include 'templates/group_form.tpl.php';
