<?php

include 'libraries/products.class.php';
$productsObj = new Products();

include 'libraries/manufacturer.class.php';
$manufacturersObj = new Manufacturers();

// sukuriame sutarčių klasės objektą
include 'libraries/group.class.php';
$groupsObj = new Groups();

$data = array();
$formErrors = null;

// nustatome privalomus formos laukus
$required = array('name', 'active', 'price', 'stockLeft');

// maksimalūs leidžiami laukų ilgiai
$maxLengths = array (
	'name' => 50,
	'nameLatin' => 50,
	'quantity' => 10,
	'rating' => 10,
	'active' => 1,
	'price' => 10,
	'stockLeft' => 10,
	'description' => 50,
	'fk_Manufacturers' => 10,
	'fk_Groups' => 10
);

// vartotojas paspaudė išsaugojimo mygtuką
if(!empty($_POST['submit'])) {
	include 'utils/validator.class.php';

	// nustatome laukų validatorių tipus
	$validations = array (
		'name' => 'alfanum',
		'nameLatin' => 'alfanum',
		'quantity' => 'positivenumber',
		'rating' => 'positivenumber',
		'active' => 'positivenumber',
		'price' => 'price',
		'stockLeft' => 'positivenumber',
		'description' => 'alfanum',
		'fk_Manufacturers' => 'alfanum',
		'fk_Groups' => 'alfanum'
	);

	// sukuriame laukų validatoriaus objektą
	$validator = new validator($validations, $required, $maxLengths);

	// laukai įvesti be klaidų
	if($validator->validate($_POST)) {
		// suformuojame laukų reikšmių masyvą SQL užklausai
		$dataPrepared = $validator->preparePostFieldsForSQL();

		//įrašome naują klientą
		$productsObj->addProduct($dataPrepared);

		//// nukreipiame vartotoją į klientų puslapį
		common::redirect("index.php?module={$module}&action=list");
		die();
	}
	else {

		$formErrors = "Aprodojant duomenis įvyko klaida, patikrinkite:";
		$formErrors .= $validator->getErrorHTML();
	}
}

// įtraukiame šabloną
include 'templates/product_form.tpl.php';
