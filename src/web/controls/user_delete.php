<?php

include 'libraries/users.class.php';
$usersObj = new Users();

if(!empty($id)) {
	$usersObj->deleteUser($id);

	common::redirect("index.php?module={$module}&action=list");
	exit;
}

