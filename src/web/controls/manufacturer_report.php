<?php

// sukuriame sutarčių klasės objektą
include 'libraries/manufacturer.class.php';
$manufacturersObj = new Manufacturers();

include 'libraries/products.class.php';
$productsObj = new Products();

$formErrors = null;
$fields = array();
$formSubmitted = false;

$data = array();
if (empty($_POST['submit'])) {

	// rodome ataskaitos parametrų įvedimo formą
	include 'templates/manufacturer_report_form.tpl.php';
} else {
	$formSubmitted = true;

	// nustatome laukų validatorių tipus
	$validations = array (
			'manufacturer' => 'positivenumber'
	);

	// sukuriame validatoriaus objektą
	include 'utils/validator.class.php';
	$validator = new validator($validations);


	if ($validator->validate($_POST)) {
		// suformuojame laukų reikšmių masyvą SQL užklausai
		$data = $validator->preparePostFieldsForSQL();

		// išrenkame ataskaitos duomenis
		$productsReport = $manufacturersObj->getProductsReport($data['manufacturer']);
		$productsReportstats = $manufacturersObj->getProductsReportStats($data['manufacturer']);
		$manufacturer = $manufacturersObj->getManufacturer($data['manufacturer']);

		include 'templates/manufacturer_report_show.tpl.php';
	} else {
		// gauname klaidų pranešimą
		$formErrors = $validator->getErrorHTML();
		// gauname įvestus laukus
		$fields = $_POST;

		// rodome ataskaitos parametrų įvedimo formą su klaidomis ir sustabdome scenarijaus vykdym1
		include 'templates/manufacturer_report_form.tpl.php';
	}
}

