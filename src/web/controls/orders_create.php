<?php

include 'libraries/orders.class.php';
$ordersObj = new Orders();

include 'libraries/products.class.php';
$productsObj = new Products();

$data = array();
$formErrors = null;

// nustatome privalomus formos laukus
$required = array('name', 'active', 'price', 'stockLeft');

// maksimalūs leidžiami laukų ilgiai
$maxLengths = array (
	'description' => 255
);

// vartotojas paspaudė išsaugojimo mygtuką
if(!empty($_POST['submit'])) {
	include 'utils/validator.class.php';

	// nustatome laukų validatorių tipus
	$validations = array (
		'description' => 'alfanum',
		'products' => 'positivenumber'
	);

	// sukuriame laukų validatoriaus objektą
	$validator = new validator($validations, $required, $maxLengths);

	// laukai įvesti be klaidų
	if($validator->validate($_POST)) {
		// suformuojame laukų reikšmių masyvą SQL užklausai
		$dataPrepared = $validator->preparePostFieldsForSQL();

		//create order product's list
		$orderProducts = array_count_values($dataPrepared['products']);
		//get server product list
		$productList = $productsObj->getSmallProductList();

		//return sum || status
		$status = $ordersObj->validateProducts($orderProducts, $productList);

		if ($status == -2) {
			$formErrors = "Sandelyje yra nepakankamai atsargų, suformuluokite užsakimą iš naujo";
			$formErrors .= $validator->getErrorHTML();
		} elseif ($statu == -1) {
			$formErrors = "Netinkamai pasirinkta prekė:";
			$formErrors .= $validator->getErrorHTML();
		} else {
			/* EVERYTHING IS OKEY */

			//create orders
			$ordersObj->addNewOrder($status);
			$lastInsertedId = mysql::getLastInsertedId();


			//Create message
			if ($dataPrepared['description'] !== '') {
				echo "pushinammm ";
				$ordersObj->addNewOrderMessage($lastInsertedId,
					$dataPrepared['description']);
			}

			//create new OrderItem
			foreach($orderProducts as $product => $val) {
				$ordersObj->addNewOrderItem($lastInsertedId,
					$product, $val);
				$productsObj->reduceAmount($product, $val);
			}

			//nukreipiame vartotoją į klientų puslapį
			common::redirect("index.php?module={$module}&action=list");
			die();
		}
	}
	else {

		$formErrors = "Aprodojant duomenis įvyko klaida, patikrinkite:";
		$formErrors .= $validator->getErrorHTML();
	}
}

// įtraukiame šabloną
include 'templates/order_form.tpl.php';
