<?php

// sukuriame sutarčių klasės objektą
include 'libraries/group.class.php';
$groupsObj = new Groups();

// sukuriame puslapiavimo klasės objektą
include 'utils/paging.class.php';
$paging = new Paging(General::NUMBER_OF_ROWS_IN_PAGE);

// suformuojame sąrašo puslapius
$paging->process($groupsObj->getGroupCount(), $pageId);

// išrenkame nurodyto puslapio sutartis
$data = $groupsObj->getGroupList();

// įtraukiame šablon
include 'templates/group_list.tpl.php';
