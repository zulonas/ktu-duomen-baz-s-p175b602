<?php

include 'libraries/users.class.php';
$usersObj = new Users();

$formErrors = null;
$data = array();
// nustatome privalomus formos laukus
$required = array('email', 'password');

// maksimalūs leidžiami laukų ilgiai
$maxLengths = array (
	'firstName' => 50,
	'lastName' => 50,
	'email' => 254,
	'password' => 64,
	'verified' => 1,
	'userType' => 1,
	'mobile' => 15,
	'city' => 50,
	'postCode' => 50,
	'street' => 50,
	'apartment' => 20
);

// vartotojas paspaudė išsaugojimo mygtuką
if(!empty($_POST['submit'])) {
	include 'utils/validator.class.php';

	// nustatome laukų validatorių tipus
	$validations = array (
		'firstName' => 'alfanum',
		'lastName' => 'alfanum',
		'email' => 'email',
		'password' => 'alfanum',
		'verified' => 'positivenumber',
		'userType' => 'positivenumber',
		'mobile' => 'alfanum',
		'city' => 'alfanum',
		'postCode' => 'positivenumber',
		'street' => 'alfanum',
		'apartment' => 'alfanum'
	);

	// sukuriame laukų validatoriaus objektą
	$validator = new validator($validations, $required, $maxLengths);

	// laukai įvesti be klaidų
	if($validator->validate($_POST)) {
		// suformuojame laukų reikšmių masyvą SQL užklausai
		$dataPrepared = $validator->preparePostFieldsForSQL();

		// įrašome naują klientą
		$usersObj->editUser($dataPrepared);

		//// nukreipiame vartotoją į klientų puslapį
		common::redirect("index.php?module={$module}&action=list");
		die();
	}
	else {

		$formErrors = "Aprodojant duomenis įvyko klaida, patikrinkite:";
		$formErrors .= $validator->getErrorHTML();
	}
} else {
	$data = $usersObj->getUser($id);
}

// nustatome požymį, kad įrašas redaguojamas norint išjungti ID redagavimą šablone
$data['editing'] = 1;

// įtraukiame šabloną
include 'templates/user_edit.tpl.php';
