<?php

include 'libraries/products.class.php';
$productsObj = new Products();

if(!empty($id)) {
	$productsObj->deleteProduct($id);

	common::redirect("index.php?module={$module}&action=list");
	die();
}
