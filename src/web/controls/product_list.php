<?php

// sukuriame sutarčių klasės objektą
include 'libraries/products.class.php';
$productsObj = new Products();

// sukuriame puslapiavimo klasės objektą
include 'utils/paging.class.php';
$paging = new Paging(General::NUMBER_OF_ROWS_IN_PAGE);

// suformuojame sąrašo puslapius
$paging->process($productsObj->getProductCount(), $pageId);

// išrenkame nurodyto puslapio sutartis
$data = $productsObj->getProductList();

// įtraukiame šabloną
include 'templates/product_list.tpl.php';
