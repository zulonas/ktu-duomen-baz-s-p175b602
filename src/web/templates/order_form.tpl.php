<h1 class="mt-5"><?php if(empty($id)) echo "Naujo užsakymo sukūrimas"; else echo "Užsakymo redagavimas"; ?></h1>

<div class="pt-4 row justify-content-center">
 <div class="col-sm-6 border border-light p-3 rounded pb-0">
	<?php if($formErrors) { ?>
		<div class="alert alert-warning" role="alert">
			<?php
				echo $formErrors;
			?>
		</div>
	<?php } ?>

	<form action="" method="post">
		<!--
		<div class="form-group">
			<?php if(!empty($id)) { ?>
				<label>ID:</label>
				<input name="id" type="text" class="form-control" value="<?php echo $data['id']; ?>" readonly>
			<?php } ?>
		</div>
		<div class="form-group">
			<label>Kaina*:</label>
			<?php
				if(empty($id)) {
					echo '<input name="price" type="text" class="form-control" value="" required>';
				} else {
					echo '<input name="price" type="text" class="form-control" value="'.  ((!empty($data['price']))? $data['price'] : '') . '">';
				}
			?>
		</div>
		-->
		<div class="form-group">
			<label>Pastabos apie užsakymą:</label>
			<?php
				if(empty($id)) {
					echo '<textarea name="description" type="text" class="form-control imputlg" value=""></textarea>';
				} else {
					echo '<textarea name="description" type="text" class="form-control" value="">'
						. ((!empty($data['description']))? $data['description'] : '') . '</textarea>';
				}
			?>
		</div>

		<div class="form-group">
			<label>Prekės:</label>
			<div class="bg-light rounded p-2">
				<div class="childRowContainer">
					<div class="pl-2 childRow hidden form-row my-1">
						<select id="inputState" name="products[]" class="form-control col-md-10">
						      <option value="-1">Pasirinkite prekę...</option>
						      <?php
							      // išrenkame visas markes
							      $products = $productsObj->getSmallProductList();
							      foreach($products as $key => $val) {
								      $selected = "";
								      echo "<option value='{$val['id']}'>{$val['name']}, {$val['price']}&euro;</option>";
							      }
						      ?>
					        </select>
						<a href="#" title="" class="removeChild col-md-1">šalinti</a>
					</div>
				</div>
				<p id="newItemButtonContainer" class="pt-2 pl-2">
					<a href="#" title="" class="addChild">Pridėti</a>
				</p>
			</div>
		</div>

		<div class="form-group float-right">
			<button name="submit" type="submit" value="submit" class="btn btn-primary">Išsaugoti</button>
		</div>
	</form>
 </div>
</div>
