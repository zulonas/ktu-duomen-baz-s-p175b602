<h1 class="mt-5">Ataskaitos formavimas</h1>

<div class="pt-4 row justify-content-center">
 <div class="col-sm-6 border border-light p-3 rounded pb-0">
	<?php if($formErrors) { ?>
		<div class="alert alert-warning" role="alert">
			<?php
				echo $formErrors;
			?>
		</div>
	<?php } ?>


	<form action="" method="post">
		<div class="form-group">
			<label>Gamintojas:*</label>
			<select class="custom-select" id="brand" name="manufacturer">
				<option value="1">-</option>
				<?php
					// išrenkame visas markes
					$manufacturers = $manufacturersObj->getManufacturerList();
					foreach($manufacturers as $key => $val) {
						$selected = "";
						if(isset($data['fk_Manufacturers']) && $data['fk_Manufacturers'] == $val['id']) {
							$selected = " selected='selected'";
						}
						echo "<option{$selected} value='{$val['id']}'>{$val['name']}</option>";
					}
				?>
			</select>
		</div>
		<div class="form-group float-right">
			<button name="submit" type="submit" value="login" class="btn btn-primary">Formuoti</button>
		</div>
	</form>
 </div>
</div>
