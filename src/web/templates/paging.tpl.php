<ul class="pagination justify-content-center pr-2">
    <li class="page-item">
      <a class="page-link" href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>
	<?php foreach ($paging->data as $key => $value) {
		$activeClass = $value['isActive'] == 1 ? "active" :  "";
		echo "
			<li class='page-item {$activeClass}'>
				<a class='page-link' href='index.php?module={$module}&action=list&page={$value['page']}'> {$value['page']}</a>
			</li>";
	} ?>
	<li class="page-item">
      <a class="page-link" href="#" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
</ul>
