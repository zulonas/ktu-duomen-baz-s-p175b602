<h1 class="mt-5">Vartotojo redagavimas</h1>

<div class="pt-4 row justify-content-center">
 <div class="col-sm-6 border border-light p-3 rounded">
	<?php if($formErrors) { ?>
		<div class="alert alert-warning" role="alert">
			<?php
				echo $formErrors;
			?>
		</div>
	<?php } ?>

	<form action="" method="post">
		<div class="form-group">
			<label>Vartotojo ID:*</label>
			<input name="id" class="form-control" value="<?php echo $data['id']; ?>" aria-describedby="emailHelp" readonly>
		</div>
		<div class="form-group">
			<label>El. paštas:*</label>
			<input type="text" class="form-control" value="<?php echo $data['email']; ?>" aria-describedby="emailHelp" readonly>
		</div>
		<div class="form-group">
			<label>Vardas:</label>
			<input name="firstName" type="text" class="form-control" value="<?php echo ((!empty($data['firstName']))? $data['firstName'] : ''); ?>">
		</div>
		<div class="form-group">
			<label>Pavardė:</label>
			<input name="lastName" type="text" class="form-control" value="<?php echo $data['lastName']; ?>">
		</div>
		<div class="form-group">
			<label>Aktyvuotas</label>
			<select class="custom-select" name="verified">
			  <?php if($data['verified']) { ?>
				  <option value="0">Ne</option>
				  <option value="1" selected>Taip</option>
			  <?php } else { ?>
				  <option value="0" selected>Ne</option>
				  <option value="1">Taip</option>
			  <?php } ?>
			</select>
		</div>
		<div class="form-group">
			<label>Tipas</label>
			<select class="custom-select" name="userType">
			  <?php if($data['userType']) { ?>
				  <option value="0">Paprastas naudotojas</option>
				  <option value="1" selected>Administratorius</option>
			  <?php } else { ?>
				  <option value="0" selected>Paprastas naudotojas</option>
				  <option value="1">Administratorius</option>
			  <?php } ?>
			</select>
		</div>
		<div class="form-group">
			<label>Telefonas:</label>
			<input name="mobile" type="text" value="<?php echo ((!empty($data['mobile']))? $data['mobile'] : '');  ?>" class="form-control">
		</div>
		<div class="form-group">
			<label>Miestas:</label>
			<input name="city" type="text" value="<?php echo $data['city']; ?>" class="form-control">
		</div>
		<div class="form-group">
			<label>Pašto kodas:</label>
			<input name="postCode" type="text" value="<?php echo $data['postCode']; ?>" class="form-control">
		</div>
		<div class="form-group">
			<label>Gatvė:</label>
			<input name="street" type="text" value="<?php echo $data['street']; ?>" class="form-control">
		</div>
		<div class="form-group">
			<label>Apartamentas:</label>
			<input name="apartment" type="text" value="<?php echo $data['apartment']; ?>" class="form-control">
		</div>
		<div class="form-group">
			<label>Paskutinį kartą prisijungta:*</label>
			<input type="text" class="form-control" value="<?php echo $data['lastLogin']; ?>" readonly>
		</div>
		<div class="form-group">
			<label>Paskutinis IP adresas:</label>
			<input type="text" class="form-control" value="<?php echo $data['lastIP']; ?>" readonly>
		</div>
		<div class="form-group">
			<label>Registruotas:</label>
			<input type="text" class="form-control" value="<?php echo $data['registeredAt']; ?>" readonly>
		</div>
		<!--
		<div class="form-group">
			<label >Gimimo data:</label>
			<div class="input-group date" data-provide="datepicker">
				<input type="text" class="form-control">
				<div class="input-group-addon">
					<span class="glyphicon glyphicon-th"></span>
				</div>
			</div>
		</div>
		-->
		<div class="form-group float-right">
			<button name="submit" type="submit" value="login" class="btn btn-primary">Išsaugoti</button>
		</div>
	</form>
 </div>
</div>
