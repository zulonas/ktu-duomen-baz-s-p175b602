<!DOCTYPE html>
<html>
<head>
  <!-- required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- bootstrap css -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" href="style/css/main.css" />
  <title><?php echo General::WEBSITE_TITLE; ?></title>
</head>
<body>
  <header>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
	<a class="navbar-brand mr-md-auto" href="index.php?module=front&action=page"><?php echo General::WEBSITE_TITLE; ?></a>
	<ul class="navbar-nav">
	<?php
		echo '<li class="nav-item ' . (($module === 'front')?"underline":"").
		' "><a class="text-white p-2" href="index.php?module=front&action=page">Pradžia</a></li>' .
				 '<li class="nav-item ' . (($module === 'product')?"underline":"").
		' "><a class="text-white p-2" href="index.php?module=product&action=list">Sėklos</a></li>' .
				 '<li class="nav-item ' . (($module === 'manufacturer')?"underline":"").
		' "><a class="text-white p-2" href="index.php?module=manufacturer&action=list">Gamintojai</a></li>'.
				 '<li class="nav-item ' . (($module === 'orders')?"underline":"").
				 ' "><a class="text-white p-2" href="index.php?module=orders&action=list">Užsakymai</a></li>';



		if ($_SESSION["loggedin"] === true) {
			echo '<li class="nav-item ' . (($module === 'group')?"underline":"").
				' "><a class="text-white p-2" href="index.php?module=group&action=list">Grupės</a></li>';
			echo '<li class="nav-item ' . (($module === 'user')?"underline":"").
				' "><a class="text-white p-2" href="index.php?module=user&action=list">Vartotojai</a></li>';
			echo '<li class="nav-item ' . (($module === 'report')?"underline":"").
				' "><a class="text-white p-2 font-weight-bold" href="index.php?module=report&action=list">Ataskaitos</a></li>';
		}
	?>
	</ul>
	<?php
		if ($_SESSION["loggedin"] === true) {
			echo '<a class="ml-2 btn btn-light">' . $_SESSION["email"] . ' </a>' .
				   '<a class="ml-2 btn btn-primary" href="index.php?module=user&action=logout">Atsijungti</a>';
		} else {
			echo '<a class="ml-2 btn btn-primary" href="index.php?module=user&action=login">Prisijungti</a>';
		}
	?>
  </header>

  <main role="main" class="container">
    <?php
      if (file_exists($actionFile)) {
        include $actionFile;
      }
    ?>
	</main>

  <footer class="footer pull-right">
	<div class="container text-right">
		<span class="text-muted">KTU, Zulonas 2020</span>
	</div>
  </footer>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
    integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
    crossorigin="anonymous"></script>
  <script
    src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
    crossorigin="anonymous"></script>
  <script
    src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
    integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
    crossorigin="anonymous"></script>
  <script type="text/javascript" src="scripts/main.js"></script>
</body>
</html>
