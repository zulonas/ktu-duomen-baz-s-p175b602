<h1 class="mt-5">Sėklos</h1>

<div class="justify-content-end row pr-4">
	<a href='index.php?module=<?php echo $module; ?>&action=create' class="btn bg-primary float-right text-white">Pridėti &plus;</a>
</div>

<div class="mt-2 border border-light p-2 rounded">
<table class="table rounded">
  <thead class="thead-light">
    <tr>
      <th scope="col">Produkto kodas</th>
      <th scope="col">Pavadinimas</th>
      <th scope="col">Lot. pavadinimas</th>
	  <th scope="col">Gamintojas</th>
      <th scope="col">Likutis</th>
      <th scope="col">Kaina</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
<?php
	//suformuojame lentelę
	foreach($data as $key => $val) {
		echo
			"<tr>"
			. "<td scope=\"row\">{$val['id']}</td>"
			. "<td>{$val['name']}</td>"
			. "<td>{$val['nameLatin']}</td>"
			. "<td>{$val['manufacturer']}</td>"
			. "<td>{$val['stockLeft']}</td>"
			. "<td>{$val['price']} &euro;</td>"
			. "<td><a href='#' onclick='showConfirmDialog(\"{$module}\", \"{$val['id']}\"); return false;' class='btn btn-danger' aria-label='Close'><span aria-hidden='true'>&times;</span></a>"
			. "<a href='index.php?module={$module}&action=edit&id={$val['id']}' class='btn btn-warning ml-1' aria-label='Close'><span aria-hidden='true'>&equiv;</span></a></td>"
			. "</tr>";
	}
	?>
  </tbody>
</table>

<?php include 'templates/paging.tpl.php'; ?>

</div>
