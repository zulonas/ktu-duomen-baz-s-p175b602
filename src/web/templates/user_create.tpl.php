<h1 class="mt-5">Naujo vartotojo kūrimas</h1>

<div class="pt-4 row justify-content-center">
 <div class="col-sm-6 border border-light p-3 rounded">
	<?php if($formErrors) { ?>
		<div class="alert alert-warning" role="alert">
			<?php
				echo $formErrors;
			?>
		</div>
	<?php } ?>

	<form action="" method="post">
		<div class="form-group">
			<label>El. paštas:*</label>
			<input name="email" type="text" class="form-control"  aria-describedby="emailHelp" required>
		</div>
		<div class="form-group">
			<label>Slaptažoodis:*</label>
			<input name="password" type="password" class="form-control" required>
		</div>
		<div class="form-group">
			<label>Vardas:</label>
			<input name="firstName" type="text" class="form-control">
		</div>
		<div class="form-group">
			<label>Pavardė:</label>
			<input name="lastName" type="text" class="form-control">
		</div>
		<div class="form-group">
			<label>Aktyvuotas</label>
			<select class="custom-select" name="verified">
			  <option value="0" selected>Ne</option>
			  <option value="1">Taip</option>
			</select>
		</div>
		<div class="form-group">
			<label>Tipas</label>
			<select class="custom-select" name="userType">
			  <option value="0" selected>Paprastas naudotojas</option>
			  <option value="1">Administratorius</option>
			</select>
		</div>
		<div class="form-group">
			<label>Telefonas:</label>
			<input name="mobile" type="text" class="form-control">
		</div>
		<div class="form-group">
			<label>Miestas:</label>
			<input name="city" type="text" class="form-control">
		</div>
		<div class="form-group">
			<label>Pašto kodas:</label>
			<input name="postCode" type="text" class="form-control">
		</div>
		<div class="form-group">
			<label>Gatvė:</label>
			<input name="steet" type="text" class="form-control">
		</div>
		<div class="form-group">
			<label>Apartamentas:</label>
			<input name="apartament" type="text" class="form-control">
		</div>
		<!--
		<div class="form-group">
			<label >Gimimo data:</label>
			<div class="input-group date" data-provide="datepicker">
				<input type="text" class="form-control">
				<div class="input-group-addon">
					<span class="glyphicon glyphicon-th"></span>
				</div>
			</div>
		</div>
		-->
		<div class="form-group float-right">
			<button name="submit" type="submit" value="login" class="btn btn-primary">Sukurti</button>
		</div>
	</form>
 </div>
</div>


<!--

  `id` int NOT NULL AUTO_INCREMENT,
  `firstName` varchar(50) DEFAULT NULL,
  `lastName` varchar(50) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `password` varchar(64) NOT NULL,
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `userType` int NOT NULL DEFAULT '0',
  `mobile` varchar(15) DEFAULT NULL,
  `birthDate` date DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `postCode` varchar(50) DEFAULT NULL,
  `street` varchar(50) DEFAULT NULL,
  `apartment` varchar(50) DEFAULT NULL,
  `lastLogin` datetime NOT NULL,
  `lastIP` varchar(45) NOT NULL,
-->
