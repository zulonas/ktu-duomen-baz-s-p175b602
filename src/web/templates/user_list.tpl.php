<h1 class="mt-5">Vartotojai</h1>

<div class="justify-content-end row pr-4">
	<a href='index.php?module=<?php echo $module; ?>&action=create' class="btn bg-primary float-right text-white">Naujas &plus;</a>
</div>

<div class="mt-2 border border-light p-2 rounded">
<table class="table rounded">
  <thead class="thead-light">
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Vardas Pavardė</th>
      <th scope="col">El. paštas</th>
	  <th scope="col">Patvirtintas</th>
      <th scope="col">IP adresas</th>
	  <th scope="col">Paskutinis prisijungimas</th>
	  <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
	<?php
	//suformuojame lentelę
	foreach($data as $key => $val) {
		echo
			"<tr>"
			. "<td scope=\"row\">{$val['id']}</td>"
			. "<td>" . ((!empty($val['firstName']))? $val['firstName'] : '')
			. " " . ((!empty($val['lastName']))? $val['lastName'] : '') . "</td>"
			. "<td>{$val['email']}</td>"
			. "<td>" . (($val['verified'])?"taip":"ne") . "</td>"
			. "<td>{$val['lastIP']}</td>"
			. "<td>{$val['lastLogin']}</td>"
			. "<td><a href='#' onclick='showConfirmDialog(\"{$module}\", \"{$val['id']}\"); return false;' class='btn btn-danger' aria-label='Close'><span aria-hidden='true'>&times;</span></a>"
			. "<a href='index.php?module={$module}&action=edit&id={$val['id']}' class='btn btn-warning ml-1' aria-label='Close'><span aria-hidden='true'>&equiv;</span></a></td>"
			. "</tr>";
	}
	?>
  </tbody>
</table>

<?php include 'templates/paging.tpl.php'; ?>

</div>
