<h1 class="mt-5 mb-3">Produktų/gamintojų ataskaita</h1>

<h6 class="p-2"><b>Filtruojama pagal:</b> <?php echo $manufacturer['name'] ?></h6> 
<div class="mt-2 border border-light p-2 rounded">
<table class="table rounded">
  <thead class="thead-light">
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Pavadinimas</th>
      <th scope="col">Kiekis</th>
      <th scope="col">Suma</th>
    </tr>
  </thead>
  <tbody>
	<?php
	//suformuojame lentelę
	foreach($productsReport as $key => $val) {
		echo
			"<tr>"
			. "<td scope=\"row\">{$val['id']}</td>"
			. "<td>{$val['name']}</td>"
			. "<td>{$val['amount']}</td>"
			. "<td>{$val['totalSum']} &euro;</td>"
			. "</tr>";
	}
	?>
    
	<?php if(count($productsReport)) { ?>
	    <tr class="thead-light">
	      <th scope="col"></th>
	      <th scope="col"></th>
	      <th scope="col">Bendras kiekis</th>
	      <th scope="col">Bendra suma</th>
	    </tr>
	    <tr class="">
	      <th scope="col"></th>
	      <th scope="col"></th>
	      <th scope="col"><?php echo $productsReportstats['totalAmount'] ?></th>
	      <th scope="col"><?php echo $productsReportstats['totalSum'] ?> &euro;</th>
	    </tr>
	<?php } else { ?>
	    <tr>
	      <th scope="col">Nebuvo atlikta jokių pirkimų</th>
	    </tr>
	<?php } ?>
  </tbody>
</table>

</div>
