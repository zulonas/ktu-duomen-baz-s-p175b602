<h1 class="mt-5 text-center">Prisijungimas</h1>

<div class="pt-4 row justify-content-center">
 <div class="col-sm-6 border border-light p-3 rounded">
	<?php if($formErrors) { ?>
		<div class="alert alert-warning" role="alert">
			<?php
				echo $formErrors;
			?>
		</div>
	<?php } ?>

	<form action="" method="post">
		<div class="form-group">
			<label for="exampleInputEmail1">El. paštas:</label>
			<input name="email" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required>
		</div>
		<div class="form-group">
			<label for="exampleInputPassword1">Slaptažoodis:</label>
			<input name="password" type="password" class="form-control" id="exampleInputPassword1" required>
		</div>
		<div class="form-group float-right">
			<button name="submit" type="submit" value="login" class="btn btn-primary">Prisjungti</button>
			<button name="submit" type="submit" value="register" class="btn btn-info">Registruotis</button>
		</div>
	</form>
 </div>
</div>
