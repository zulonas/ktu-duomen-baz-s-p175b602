<h1 class="mt-5"><?php if(empty($id)) echo "Sėklų pridėjimas"; else echo "Sėklų redagavimas"; ?></h1>

<div class="pt-4 row justify-content-center">
 <div class="col-sm-6 border border-light p-3 rounded pb-0">
	<?php if($formErrors) { ?>
		<div class="alert alert-warning" role="alert">
			<?php
				echo $formErrors;
			?>
		</div>
	<?php } ?>

	<form action="" method="post">
		<div class="form-group">
			<?php if(!empty($id)) { ?>
				<label>ID:</label>
				<input name="id" type="text" class="form-control" value="<?php echo $data['id']; ?>" readonly>
			<?php } ?>
		</div>
		<div class="form-group">
			<label>Pavadinimas:*</label>
			<?php
				if(empty($id)) {
					echo '<input name="name" type="text" class="form-control" value="" required>';
				} else {
					echo '<input name="name" type="text" class="form-control" value="'.  ((!empty($data['name']))? $data['name'] : '') . '">';
				}
			?>
		</div>
		<div class="form-group">
			<label>Lotyniškas pavadinimas:</label>
			<?php
				if(empty($id)) {
					echo '<input name="nameLatin" type="text" class="form-control" value="">';
				} else {
					echo '<input name="nameLatin" type="text" class="form-control" value="'.  ((!empty($data['nameLatin']))? $data['nameLatin'] : '') . '">';
				}
			?>
		</div>
		<div class="form-group">
			<label>Kiekis:</label>
			<?php
				if(empty($id)) {
					echo '<input name="quantity" type="text" class="form-control" value="">';
				} else {
					echo '<input name="quantity" type="text" class="form-control" value="'.  ((!empty($data['quantity']))? $data['quantity'] : '') . '">';
				}
			?>
		</div>
		<div class="form-group">
			<?php if(!empty($id)) { ?>
				<label>Vertinimas:</label>
				<input type="text" class="form-control" value="<?php echo $data['rating']; ?>" readonly>
			<?php } ?>
		</div>
		<div class="form-group">
			<label>Aktyvus:</label>
			<select class="custom-select" name="active">
			  <?php if($data['active']) { ?>
				  <option value="0">Ne</option>
				  <option value="1" selected>Taip</option>
			  <?php } else { ?>
				  <option value="0" selected>Ne</option>
				  <option value="1">Taip</option>
			  <?php } ?>
			</select>
		</div>
		<div class="form-group">
			<label>Kaina*:</label>
			<?php
				if(empty($id)) {
					echo '<input name="price" type="text" class="form-control" value="" required>';
				} else {
					echo '<input name="price" type="text" class="form-control" value="'.  ((!empty($data['price']))? $data['price'] : '') . '">';
				}
			?>
		</div>
		<div class="form-group">
			<label>Likutis*:</label>
			<?php
				if(empty($id)) {
					echo '<input name="stockLeft" type="text" class="form-control" value="" required>';
				} else {
					echo '<input name="stockLeft" type="text" class="form-control" value="'.  ((!empty($data['stockLeft']))? $data['stockLeft'] : '') . '">';
				}
			?>
		</div>
		<div class="form-group">
			<label>Aprašymas:</label>
			<?php
				if(empty($id)) {
					echo '<textarea name="description" type="text" class="form-control imputlg" value=""></textarea>';
				} else {
					echo '<textarea name="description" type="text" class="form-control" value="">'
						. ((!empty($data['description']))? $data['description'] : '') . '</textarea>';
				}
			?>
		</div>
		<div class="form-group">
			<label>Gamintojas:*</label>
			<select class="custom-select" id="brand" name="fk_Manufacturers">
				<option value="1">-</option>
				<?php
					// išrenkame visas markes
					$manufacturers = $manufacturersObj->getManufacturerList();
					foreach($manufacturers as $key => $val) {
						$selected = "";
						if(isset($data['fk_Manufacturers']) && $data['fk_Manufacturers'] == $val['id']) {
							$selected = " selected='selected'";
						}
						echo "<option{$selected} value='{$val['id']}'>{$val['name']}</option>";
					}
				?>
			</select>
		</div>
		<div class="form-group">
			<label>Grupė:*</label>
			<select class="custom-select" id="brand" name="fk_Groups">
				<option value="1">-</option>
				<?php
					// išrenkame visas markes
					$groups = $groupsObj->getGroupList();
					foreach($groups as $key => $val) {
						$selected = "";
						if(isset($data['fk_Groups']) && $data['fk_Groups'] == $val['id']) {
							$selected = " selected='selected'";
						}
						echo "<option{$selected} value='{$val['id']}'>{$val['name']}</option>";
					}
				?>
			</select>
		</div>
		<div class="form-group float-right">
			<button name="submit" type="submit" value="login" class="btn btn-primary">Išsaugoti</button>
		</div>
	</form>
 </div>
</div>
