<h1 class="mt-5">Grupės</h1>


<div class="justify-content-end row pr-4">
	<a href='index.php?module=<?php echo $module; ?>&action=create' class="btn bg-primary float-right text-white">Nauja &plus;</a>
</div>

<?php if(isset($_GET['remove_error'])) { ?>
	<div class="mt-3 alert alert-warning" role="alert">
		Grupė nebuvo pašalinta, nes jei yra priskirta prekių.
	</div>
<?php } ?>

<div class="mt-2 border border-light p-2 rounded">
<table class="table rounded">
  <thead class="thead-light">
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Pavadinimas</th>
      <th scope="col">Atributai</th>
	  <th scope="col">Aprašymas</th>
	  <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
	<?php
	//suformuojame lentelę
	foreach($data as $key => $val) {
		echo
		"<tr>"
		. "<td scope=\"row\">{$val['id']}</td>"
		. "<td>" . ((!empty($val['name']))? $val['name'] : '') . "</td>"
		. "<td>" . ((!empty($val['attributes']))? $val['attributes'] : '') . "</td>"
		. "<td>" . ((!empty($val['description']))? $val['description'] : '') . "</td>"
		. "<td><a href='#' onclick='showConfirmDialog(\"{$module}\", \"{$val['id']}\"); return false;' class='btn btn-danger' aria-label='Close'><span aria-hidden='true'>&times;</span></a>"
		. "<a href='index.php?module={$module}&action=edit&id={$val['id']}' class='btn btn-warning ml-1' aria-label='Close'><span aria-hidden='true'>&equiv;</span></a></td>"
		. "</tr>";
	}
	?>
  </tbody>
</table>

<?php include 'templates/paging.tpl.php'; ?>

</div>
