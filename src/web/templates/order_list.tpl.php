
<h1 class="mt-5">Užsakymai</h1>

<div class="justify-content-end row pr-4">
	<a href='index.php?module=<?php echo $module; ?>&action=create' class="btn bg-primary float-right text-white">Naujas &plus;</a>
</div>

<div class="mt-2 border border-light p-2 rounded">
<table class="table rounded">
  <thead class="thead-light">
    <tr>
      <th scope="col">Kodas</th>
      <th scope="col">Užsakovas</th>
      <th scope="col">Data</th>
      <th scope="col">Suma</th>
      <th scope="col">Statusas</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
<?php
	//suformuojame lentelę
	foreach($data as $key => $val) {
		echo
			"<tr>"
			. "<td scope=\"row\">{$val['id']}</td>"
			. "<td>{$val['firstName']} {$val['lastName']}</td>"
			. "<td>{$val['date']}</td>"
			. "<td>{$val['price']} &euro;</td>"
			. "<td>{$val['status']}</td>"
			. "<td><a href='#' onclick='showConfirmDialog(\"{$module}\", \"{$val['id']}\"); return false;' class='btn btn-danger' aria-label='Close'><span aria-hidden='true'>&times;</span></a></td>"
			. "</tr>";
	}
	?>
  </tbody>
</table>

<?php include 'templates/paging.tpl.php'; ?>

</div>
