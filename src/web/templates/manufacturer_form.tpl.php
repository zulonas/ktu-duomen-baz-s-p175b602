<h1 class="mt-5"><?php if(empty($id)) echo "Naujas gamintojas"; else echo "Gamintojo redagavimas"; ?></h1>

<div class="pt-4 row justify-content-center">
 <div class="col-sm-6 border border-light p-3 rounded">
	<?php if($formErrors) { ?>
		<div class="alert alert-warning" role="alert">
			<?php
				echo $formErrors;
			?>
		</div>
	<?php } ?>

	<form action="" method="post">
		<div class="form-group">
			<?php if(!empty($id)) { ?>
				<label>ID:</label>
				<input name="id" type="text" class="form-control" value="<?php echo $data['id']; ?>" readonly>
			<?php } ?>
		</div>
		<div class="form-group">
			<label>Vardas:</label>
			<?php
				if(empty($id)) {
					echo '<input name="name" type="text" class="form-control" value="">';
				} else {
					echo '<input name="name" type="text" class="form-control" value="'.  ((!empty($data['name']))? $data['name'] : '') . '">';
				}
			?>
		</div>
		<div class="form-group">
			<label>Atributai:</label>
			<?php
				if(empty($id)) {
					echo '<input name="attributes" type="text" class="form-control" value="">';
				} else {
					echo '<input name="attributes" type="text" class="form-control" value="'.  ((!empty($data['attributes']))? $data['attributes'] : '') . '">';
				}
			?>
		</div>
		<div class="form-group">
			<label>Aprašymas:</label>
			<?php
				if(empty($id)) {
					echo '<textarea name="description" type="text" class="form-control imputlg" value=""></textarea>';
				} else {
					echo '<textarea name="description" type="text" class="form-control" value="">'
						. ((!empty($data['description']))? $data['description'] : '') . '</textarea>';
				}
			?>
		</div>
		<div class="form-group float-right">
			<button name="submit" type="submit" value="login" class="btn btn-primary">Išsaugoti</button>
		</div>
	</form>
 </div>
</div>
