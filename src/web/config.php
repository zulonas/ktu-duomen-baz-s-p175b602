<?php

class Config
{
    const DB_SERVER    = 'db'; // takes from docker-compose.yml
    const DB_NAME      = 'test_db';
    const DB_USERNAME  = 'devuser';
    const DB_PASSWORD  = 'devpass';
    const DB_PREFIX    = '';
}
