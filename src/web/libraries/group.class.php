<?php

class Groups
{
	private $groups_table = '';
	private $products_table = '';

	public function __construct() {
		$this->groups_table = config::DB_PREFIX . 'Groups';
		$this->products_table = config::DB_PREFIX . 'Products';
	}

	/**
	 * Get single project from database by id
	 * @param type $id
	 * @return type
	 */
	public function getGroup($id)
	{
		$query = "  SELECT *
			FROM `{$this->groups_table}`
			WHERE `id`='{$id}'";
		$data = mysql::select($query);

		return $data[0];
	}

	public function getGroupList($limit = null, $offset = null)
	{
		$query = "  SELECT *
			FROM `{$this->groups_table}`
			WHERE `{$this->groups_table}`.`id` > 1"; /* ignore first value - default */

		if (isset($limit))
			$query .= " LIMIT {$limit}";

		if (isset($offset))
			$query .= " OFFSET {$offset}";

		$data = mysql::select($query);
		return $data;
	}

	public function deleteGroup($id) {
		$query = "  DELETE FROM `{$this->groups_table}`
			WHERE `id`='{$id}'";
		mysql::query($query);
	}

	public function getGroupCount()
	{
		$query = "SELECT COUNT(*) AS `count`
			FROM `{$this->groups_table}`";

		$data = mysql::select($query);
		return $data[0]['count'];
	}

	public function editGroup($data) {
		$query = "  UPDATE `{$this->groups_table}`
			SET    `name`=" . (!empty($data['name'])? "'" . $data['name'] . "'": 'NULL') .",
			`slug`=" . (!empty($data['slug'])? "'" . $data['slug'] . "'": 'NULL')   . ",
			`description`=" . (!empty($data['description'])? "'" . $data['description'] . "'": 'NULL')   . ",
			`attributes`=" . (!empty($data['attributes'])? "'" . $data['attributes'] . "'": 'NULL')   . "
			WHERE `id`='{$data['id']}'";
		mysql::query($query);
	}

	/**
	 * Register new user to database
	 * @param type $data
	 */
	public function addGroup($data)
	{
		$query = "  INSERT INTO `{$this->groups_table}`
			(
				`name`,
				`slug`,
				`description`,
				`attributes`
			)
			VALUES
			(
				" . (!empty($data['name'])? "'" . $data['name'] . "'": 'NULL') . ",
				" . (!empty($data['slug'])? "'" . $data['slug'] . "'": 'NULL') . ",
				" . (!empty($data['description'])? "'". $data['description'] . "'": 'NULL') . ",
				" . (!empty($data['attributes'])? "'". $data['attributes'] . "'": 'NULL') . "
			)";
		mysql::query($query);
	}

	public function getProductsCountOfGroup($id)
	{
		$query = "  SELECT COUNT(*) as amount
			FROM `{$this->products_table}`
			WHERE `{$this->products_table}`.`fk_Groups`=" . $id;
		$data = mysql::select($query);

		return $data[0]['amount'];
	}
}
