<?php

class Manufacturers
{
	private $manufacturers_table = '';
	private $products_table = '';
	private $orderItems_table = '';

	public function __construct()
	{
		$this->manufacturers_table = config::DB_PREFIX . 'Manufacturers';
		$this->products_table = config::DB_PREFIX . 'Products';
		$this->orderItems_table = config::DB_PREFIX . 'OrderItems';
	}

	public function getManufacturer($id)
	{
		$query = "  SELECT *
			FROM `{$this->manufacturers_table}`
			WHERE `id`='{$id}'";
		$data = mysql::select($query);

		return $data[0];
	}

	public function getManufacturerList($limit = null, $offset = null)
	{
		$query = "  SELECT *
			FROM `{$this->manufacturers_table}`
			WHERE `{$this->manufacturers_table}`.`id` > 1"; /* ignore first value - default */

		if (isset($limit))
			$query .= " LIMIT {$limit}";

		if (isset($offset))
			$query .= " OFFSET {$offset}";

		$data = mysql::select($query);
		return $data;
	}

	public function deleteManufacturer($id) {
		$query = "  DELETE FROM `{$this->manufacturers_table}`
			WHERE `id`='{$id}'";
		mysql::query($query);
	}

	public function getManufacturerCount()
	{
		$query = "SELECT COUNT(*) AS `count`
			FROM `{$this->manufacturers_table}`";

		$data = mysql::select($query);
		return $data[0]['count'];
	}

	public function editManufacturer($data) {
		$query = "  UPDATE `{$this->manufacturers_table}`
			SET    `name`=" . (!empty($data['name'])? "'" . $data['name'] . "'": 'NULL') .",
			`slug`=" . (!empty($data['slug'])? "'" . $data['slug'] . "'": 'NULL')   . ",
			`description`=" . (!empty($data['description'])? "'" . $data['description'] . "'": 'NULL')   . ",
			`attributes`=" . (!empty($data['attributes'])? "'" . $data['attributes'] . "'": 'NULL')   . "
			WHERE `id`='{$data['id']}'";
		mysql::query($query);
	}

	public function addManufacturer($data)
	{
		$query = "  INSERT INTO `{$this->manufacturers_table}`
			(
				`name`,
				`slug`,
				`description`,
				`attributes`
			)
			VALUES
			(
				" . (!empty($data['name'])? "'" . $data['name'] . "'": 'NULL') . ",
				" . (!empty($data['slug'])? "'" . $data['slug'] . "'": 'NULL') . ",
				" . (!empty($data['description'])? "'". $data['description'] . "'": 'NULL') . ",
				" . (!empty($data['attributes'])? "'". $data['attributes'] . "'": 'NULL') . "
			)";
		mysql::query($query);
	}

	public function getProductsCountOfManufacturer($id)
	{
		$query = "  SELECT COUNT(*) as amount
			FROM `{$this->products_table}`
			WHERE `{$this->products_table}`.`fk_Manufacturers`=" . $id;
		$data = mysql::select($query);

		return $data[0]['amount'];
	}

	public function getProductsOfManufacturer($id)
	{
		$query = "  SELECT `{$this->products_table}`.`id`,
			`{$this->products_table}`.`name`,
			`{$this->products_table}`.`nameLatin`,
			`{$this->products_table}`.`stockLeft`,
			`{$this->products_table}`.`price`
			FROM `{$this->products_table}`
			WHERE `{$this->products_table}`.`fk_Manufacturers`=" . $id;
		$data = mysql::select($query);

		return $data;
	}

	public function getProductsReport($manufacturer)
	{
		$query = "SELECT
			`{$this->products_table}`.`id`,
			`{$this->products_table}`.`name`,
			`{$this->products_table}`.`fk_Manufacturers` as `manufacturer`,
			SUM(`{$this->orderItems_table}`.`quantity`) as `amount`,
			ROUND(SUM(`{$this->products_table}`.`price` * `{$this->orderItems_table}`.`quantity`), 2) AS `totalSum`
			FROM `{$this->products_table}`
			INNER JOIN `{$this->orderItems_table}` ON `{$this->products_table}`.`id` = `{$this->orderItems_table}`.`fk_Products`";

		if ($manufacturer !== '1')
			$query .= "WHERE `{$this->products_table}`.`fk_Manufacturers` = " . $manufacturer . " ";

		$query .= "GROUP BY `{$this->products_table}`.`id`
			ORDER BY `amount` ASC";

		$data = mysql::select($query);
		return $data;
	}

	public function getProductsReportStats($manufacturer)
	{
		$query = "SELECT
			SUM(`{$this->orderItems_table}`.`quantity`) as `totalAmount`,
			ROUND(SUM(`{$this->products_table}`.`price` * `{$this->orderItems_table}`.`quantity`), 2) AS `totalSum`
			FROM `{$this->products_table}`
			INNER JOIN `{$this->orderItems_table}` ON `{$this->products_table}`.`id` = `{$this->orderItems_table}`.`fk_Products`";

		if ($manufacturer !== '1')
			$query .= "WHERE `{$this->products_table}`.`fk_Manufacturers` = " . $manufacturer . " ";

		$data = mysql::select($query);
		return $data[0];
	}
}
