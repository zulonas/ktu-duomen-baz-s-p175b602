<?php

class Orders
{
	private $orders_table = '';
	private $users_table = '';
	private $orderItems_table = '';
	private $orderMessages_table = '';
	private $products_table = '';

	public function __construct()
	{
		$this->orders_table = config::DB_PREFIX . 'Orders';
		$this->users_table = config::DB_PREFIX . 'Users';
		$this->orderItems_table = config::DB_PREFIX . 'OrderItems';
		$this->products_table = config::DB_PREFIX . 'Products';
		$this->orderMessages_table = config::DB_PREFIX . 'OrderMessages';
	}

	public function getOrderList()
	{
		$query = "  SELECT `{$this->orders_table}`.`id`,
			`{$this->users_table}`.`firstName`,
			`{$this->users_table}`.`lastName`,
			`{$this->orders_table}`.`date`,
			`{$this->orders_table}`.`price`,
			`{$this->orders_table}`.`status`
			FROM `{$this->orders_table}`
			INNER JOIN `{$this->users_table}`
			ON `{$this->orders_table}`.`fk_User`=`{$this->users_table}`.`id`
			ORDER BY `date`";
		$data = mysql::select($query);

		return $data;
	}

	public function addNewOrderItem($orderId, $productId, $amount)
	{
		$query = "  INSERT INTO `{$this->orderItems_table}`
			(
				`fk_Products`,
				`fk_Order`,
				`quantity`
			)
			VALUES
			(
				" . $productId .",
				" . $orderId . ",
				" . $amount . "
			)";
		mysql::query($query);
	}

	public function addNewOrder($price)
	{
		$query = "  INSERT INTO `{$this->orders_table}`
			(
				`date`,
				`status`,
				`price`,
				`fk_User`
			)
			VALUES
			(
				CURRENT_TIMESTAMP,
				0,
				" . $price . ",
				" . $_SESSION['id'] . "
			)";
		mysql::query($query);
	}

	public function addNewOrderMessage($orderId, $message)
	{
		$query = "  INSERT INTO `{$this->orderMessages_table}`
			(
				`fk_Order`,
				`date`,
				`message`,
				`fk_User`
			)
			VALUES
			(
				" . $orderId . ",
				CURRENT_TIMESTAMP,
				'{$message}',
				" . $_SESSION['id'] . "
			)";

		mysql::query($query);
	}

	public function getOrderCount()
	{
		$query = "SELECT COUNT(*) AS `count`
			FROM `{$this->orders_table}`";

		$data = mysql::select($query);
		return $data[0]['count'];
	}

	public function deleteOrder($id)
	{
		//delete all order items
		$query = "DELETE FROM `{$this->orderItems_table}`
			  WHERE `{$this->orderItems_table}`.`fk_Order` = " . $id;
		mysql::query($query);

		//delete all messages
		$query = "DELETE FROM `{$this->orderMessages_table}`
			  WHERE `{$this->orderMessages_table}`.`fk_Order` = " . $id;
		mysql::query($query);

		//delete order
		$query = "DELETE FROM `{$this->orders_table}`
			  WHERE `{$this->orders_table}`.`id` = " . $id;
		mysql::query($query);
	}

	/*
	 * return -1 if product does not exits
	 * return -2 if not enought amount
	 * return >0 (sum) if everything is all right
	 */
	public function validateProducts($orderProducts, $serverProducts)
	{
		$sum = 0;
		$serverProductids = array_column($serverProducts, 'price', 'id');
		$serverProductAmount = array_column($serverProducts, 'stockLeft', 'id');

		foreach($orderProducts as $product => $val) {
			// return if product does not exit
			if (!in_array($product, array_keys($serverProductids)))
				return -1;

			// return if requested amount bigger then exiting
			if ($serverProductAmount[$product] < $val)
				return -2;

			$sum += floatval($serverProductids[$product]) * $val;
		}

		return $sum;
	}
}
