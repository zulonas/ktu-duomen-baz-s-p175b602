<?php

class Login
{
	private $user_table = '';

	public function __construct() {
		$this->user_table = config::DB_PREFIX . 'Users';
	}

	/**
	 * Get hash by email
	 * @param type $email
	 * @return hashed password
	 */
	public function getHash($email)
	{
		$query = "  SELECT `{$this->user_table}`.`password`,
			`{$this->user_table}`.`id`
			FROM `{$this->user_table}`
			WHERE `{$this->user_table}`.`email`='{$email}'";

		$data = mysql::select($query);
		return $data[0];
	}

	/**
	 * Register new user to database
	 * @param type $data
	 */
	public function registerUser($data)
	{
		$hashedPassword = password_hash($data['password'], PASSWORD_DEFAULT);
		$query = "  INSERT INTO `{$this->user_table}`
			(
				`email`,
				`password`,
				`lastLogin`,
				`lastIP`
			)
			VALUES
			(
				'{$data['email']}',
				'{$hashedPassword}',
				CURRENT_TIMESTAMP(),
				'{$_SERVER['REMOTE_ADDR']}'
			)";

		mysql::query($query);
	}


	public function updateLastLogin($id)
	{
		$query = "  UPDATE `{$this->user_table}`
			SET    `lastLogin`=CURRENT_TIMESTAMP(),
			`lastIP`='{$_SERVER['REMOTE_ADDR']}'
			WHERE `id`='{$id}'";
		mysql::query($query);
	}
}
