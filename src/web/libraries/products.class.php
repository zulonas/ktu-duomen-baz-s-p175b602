<?php

class Products
{
	private $products_table = '';
	private $manufacturers_table = '';
	private $groups_table = '';

	public function __construct() {
		$this->products_table = config::DB_PREFIX . 'Products';
		$this->manufacturers_table = config::DB_PREFIX . 'Manufacturers';
		$this->groups_table = config::DB_PREFIX . 'Groups';
	}

	public function getProduct($id)
	{
		$query = "SELECT *
			FROM `{$this->products_table}`
			WHERE `id`='{$id}'";
		$data = mysql::select($query);

		return $data[0];
	}

	public function addProduct($data)
	{
		$query = "INSERT INTO `{$this->products_table}`
			(
				`name`,
				`nameLatin`,
				`quantity`,
				`active`,
				`price`,
				`stockLeft`,
				`description`,
				`fk_Manufacturers`,
				`fk_Groups`
			)
			VALUES
			(
				" . (!empty($data['name'])? "'" . $data['name'] . "'": 'NULL') . ",
				" . (!empty($data['nameLatin'])? "'" . $data['nameLatin'] . "'": 'NULL') . ",
				" . (!empty($data['quantity'])? "'". $data['quantity'] . "'": 'NULL') . ",
				" . (!empty($data['active'])? "'". $data['attributes'] . "'": '0') . ",
				" . (!empty($data['price'])? "'". $data['price'] . "'": '0') . ",
				" . (!empty($data['stockLeft'])? "'". $data['stockLeft'] . "'": '0') . ",
				" . (!empty($data['description'])? "'". $data['description'] . "'": 'description') . ",
				" . (!empty($data['fk_Manufacturers'])? "'". $data['fk_Manufacturers'] . "'": 'NULL') . ",
				" . (!empty($data['fk_Groups'])? "'". $data['fk_Groups'] . "'": 'NULL') . "
			)";
		mysql::query($query);
	}

	public function editProduct($data)
	{
		$query = "UPDATE `{$this->products_table}`
			SET    `name`=" . (!empty($data['name'])? "'" . $data['name'] . "'": 'NULL') .",
			`nameLatin`=" . (!empty($data['nameLatin'])? "'" . $data['nameLatin'] . "'": 'NULL') . ",
			`quantity`=" . (!empty($data['quantity'])? "'" . $data['quantity'] . "'": 'NULL') . ",
			`active`=" . (!empty($data['active'])? "'" . $data['active'] . "'": '0') . ",
			`price`=" . (!empty($data['price'])? "'" . $data['price'] . "'": 'NULL') . ",
			`stockLeft`=" . (!empty($data['stockLeft'])? "'" . $data['stockLeft'] . "'": 'NULL') . ",
			`description`=" . (!empty($data['description'])? "'" . $data['description'] . "'": 'NULL') . ",
			`fk_Manufacturers`=" . (!empty($data['fk_Manufacturers'])? "'" . $data['fk_Manufacturers'] . "'": 'NULL') . ",
			`fk_Groups`=" . (!empty($data['fk_Groups'])? "'" . $data['fk_Groups'] . "'": 'NULL')   . "
			WHERE `id`='{$data['id']}'";
		mysql::query($query);
	}

	public function getProductList($limit = null, $offset = null)
	{
		$query = "SELECT `{$this->products_table}`.`id`,
			`{$this->products_table}`.`name`,
			`{$this->products_table}`.`nameLatin`,
			`{$this->manufacturers_table}`.`name` AS `manufacturer`,
			`{$this->products_table}`.`stockLeft`,
			`{$this->products_table}`.`price`
			FROM `{$this->products_table}`
			INNER JOIN `{$this->manufacturers_table}`
			ON `{$this->products_table}`.`fk_Manufacturers`=`{$this->manufacturers_table}`.`id`";

		if (isset($limit))
			$query .= " LIMIT {$limit}";

		if (isset($offset))
			$query .= " OFFSET {$offset}";

		$data = mysql::select($query);
		return $data;
	}

	public function getSmallProductList()
	{
		$query = "SELECT `{$this->products_table}`.`id`,
			`{$this->products_table}`.`name`,
			`{$this->products_table}`.`stockLeft`,
			`{$this->products_table}`.`price`
			FROM `{$this->products_table}`
			WHERE `{$this->products_table}`.`stockLeft` > 0";

		$data = mysql::select($query);
		return $data;
	}

	public function getProductCount()
	{
		$query = "SELECT COUNT(*) AS `count`
			FROM `{$this->products_table}`";

		$data = mysql::select($query);
		return $data[0]['count'];
	}

	public function deleteProduct($id)
	{
		$query = "DELETE FROM `{$this->products_table}`
			WHERE `id`='{$id}'";

		mysql::query($query);
	}

	public function reduceAmount($id, $amount)
	{
		$query = "UPDATE `{$this->products_table}`
			SET `{$this->products_table}`.`stockLeft` =
			`{$this->products_table}`.`stockLeft` - " . $amount . "
			WHERE `id`='{$id}'";

		mysql::query($query);
	}
}
