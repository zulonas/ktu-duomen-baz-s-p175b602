<?php

class Users
{
	private $user_table = '';

	public function __construct()
	{
		$this->user_table = config::DB_PREFIX . 'Users';
	}

	public function getUserList($list = null, $offset = null)
	{
		$query = "  SELECT `{$this->user_table}`.`id`,
			`{$this->user_table}`.`firstName`,
			`{$this->user_table}`.`lastName`,
			`{$this->user_table}`.`email`,
			`{$this->user_table}`.`verified`,
			`{$this->user_table}`.`lastIP`,
			`{$this->user_table}`.`lastLogin`
			FROM `{$this->user_table}`";

		if (isset($limit))
			$query .= " LIMIT {$limit}";

		if (isset($offset))
			$query .= " OFFSET {$offset}";

		$data = mysql::select($query);
		return $data;
	}

	public function getUserCount()
	{
		$query = "SELECT COUNT(*) AS `count`
			FROM `{$this->user_table}`";

		$data = mysql::select($query);
		return $data[0]['count'];
	}

	public function deleteUser($id)
	{
		$query = "  DELETE FROM `{$this->user_table}`
			WHERE `id`='{$id}'";
		mysql::query($query);
	}

	public function getUser($id) {
		$query = "  SELECT *
			FROM `{$this->user_table}`
			WHERE `id`='{$id}'";
		$data = mysql::select($query);

		return $data[0];
	}

	public function editUser($data)
	{
		$query = "  UPDATE `{$this->user_table}`
			SET    `firstName`=" . (!empty($data['firstName'])? "'" . $data['firstName'] . "'": 'NULL') .",
			`lastName`=" . (!empty($data['lastName'])? "'" . $data['lastName'] . "'": 'NULL')   . ",
			`mobile`=" . (!empty($data['mobile'])? "'" . $data['mobile'] . "'": 'NULL')   . ",
			`city`=" . (!empty($data['city'])? "'" . $data['city'] . "'": 'NULL')   . ",
			`postCode`=" . (!empty($data['postCode'])? "'" . $data['postCode'] . "'": 'NULL')   . ",
			`street`=" . (!empty($data['street'])? "'" . $data['street'] . "'": 'NULL') . ",
			`apartment`=" . (!empty($data['apartment'])? "'" . $data['apartment'] . "'": 'NULL')   . ",
			`verified`=" . (!empty($data['verified'])?  $data['verified'] : '0') . ",
			`userType`=" . (!empty($data['userType'])? $data['userType']: '0') . "
			WHERE `id`='{$data['id']}'";
		mysql::query($query);
	}

	public function addUser($data)
	{
		$hashedPassword = password_hash($data['password'], PASSWORD_DEFAULT);
		$query = "  INSERT INTO `{$this->user_table}`
			(
				`firstName`,
				`lastName`,
				`email`,
				`password`,
				`verified`,
				`userType`,
				`mobile`,
				`city`,
				`postCode`,
				`street`,
				`apartment`,
				`lastLogin`,
				`lastIP`
			)
			VALUES
			(
				" . (!empty($data['firstName'])? "'" . $data['firstName'] . "'": 'NULL') . ",
				" . (!empty($data['lastName'])? "'" . $data['lastName'] . "'": 'NULL') . ",
				'{$data['email']}',
				'{$hashedPassword}',
				" . (!empty($data['verified'])?  $data['verified'] : '0') . ",
				" . (!empty($data['userType'])? $data['userType']: '0') . ",
				" . (!empty($data['mobile'])? "'". $data['mobile'] . "'": 'NULL') . ",
				" . (!empty($data['city'])? "'". $data['city'] . "'": 'NULL') . ",
				" . (!empty($data['postCode'])? "'". $data['postCode'] . "'": 'NULL') . ",
				" . (!empty($data['street'])? "'". $data['street'] . "'": 'NULL') . ",
				" . (!empty($data['apartment'])? "'". $data['apartment'] . "'": 'NULL') . ",
				CURRENT_TIMESTAMP(),
				'{$_SERVER['REMOTE_ADDR']}'
			)";

		mysql::query($query);
	}
}
