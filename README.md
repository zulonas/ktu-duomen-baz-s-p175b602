## KTU Duomenų bazės (P175B602)

If you looking for good frameworkless PHP website example - **don't use this**. This is just another broken uni project :)

*p.s. docker setup is quite good*

### Setup
```
docker-compose build
```


### Usage
```
docker-compose up -d
```
web service - http://localhost:8000/ \
mysql service - `mysql --user devuser --host 127.0.0.1  -P 6033 -p`


### Database:
Database structure was originally created with [sqldbm](sqldbm.com)
- https://app.sqldbm.com/MySQL/Edit/p103697
